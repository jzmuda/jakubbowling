package test;

import static org.junit.Assert.*;

import org.junit.Test;

import bowling.BowlingGame;

public class BowlingTest {

	@Test
	public void shouldConstructAnUnfinishedGame() {
		//given
		BowlingGame game = new BowlingGame();
		//when
		boolean result = game.isFinished();
		//then
		assertEquals(false,result);
	}
	
	@Test
	public void shouldRegisterFirstBall() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(10);
		//when
		int result = game.score();
		//then
		assertEquals(10,result);
	}
	
	@Test
	public void shouldNotRegisterNegative() {
		//given
		BowlingGame game = new BowlingGame();
		boolean exceptionCaugth=false;
		try {//when
			game.roll(-1);
		}
		catch (Exception e) {
			exceptionCaugth=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaugth);
	}
	
	@Test
	public void shouldNotRegisterEleven() {
		//given
		BowlingGame game = new BowlingGame();
		boolean exceptionCaugth=false;
		try {//when
			game.roll(11);
		}
		catch (Exception e) {
			exceptionCaugth=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaugth);
	}
	
	@Test
	public void shouldRecognizeOverTenPinsInAFrame() {
		//given
		BowlingGame game = new BowlingGame();
		boolean exceptionCaugth=false;
		try {//when
			game.roll(7);
			game.roll(4);
		}
		catch (Exception e) {
			exceptionCaugth=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaugth);
	}
	
	@Test
	public void shouldIncreaseFrameOnStrike() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(10);
		//when
		int result = game.getFrame();
		//then
		assertEquals(2,result);
	}
	
	@Test
	public void shouldIncreaseFrameAfterSecondBall() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(3);
		game.roll(0);
		//when
		int result = game.getFrame();
		//then
		assertEquals(2,result);
	}
	
	@Test
	public void shouldRecognizeRegularEnd() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <10 ; i++)
		{
			game.roll(1);
			game.roll(0);
		}
		
		//when
		boolean result = game.isFinished();
		//then
		assertEquals(true,result);
	}
	
	@Test
	public void shouldRecognizeStrike() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(10);
		//when
		boolean result = game.getStrike();
		//then
		assertEquals(true,result);
	}

	@Test
	public void shouldRecognizeNotStrike() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(9);
		//when
		boolean result = game.getStrike();
		//then
		assertEquals(false,result);
	}
	
	@Test
	public void shouldRecognizeSpare() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(5);
		game.roll(5);
		//when
		boolean result = game.getSpare();
		//then
		assertEquals(true,result);
	}
	
	@Test
	public void shouldRecognizeNotSpare() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(9);
		game.roll(0);
		//when
		boolean result = game.getSpare();
		//then
		assertEquals(false,result);
	}
	
	@Test
	public void shouldRecognizeEndroundStrike() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <10 ; i++)
		{
			game.roll(10);
		}
		
		//when
		boolean result = game.isFinished();
		//then
		assertEquals(false,result);
	}
	
	@Test
	public void shouldRecognizeEndExceptionSpares() {
		//given
		BowlingGame game = new BowlingGame();
		boolean exceptionCaught=false;
		try{
			for(int i=0; i<22 ; i++)
			{
				game.roll(5);
			}//when
		}catch(Exception e)
		{
			System.out.println(e.toString());
			exceptionCaught=true;
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void shouldRecognizeEndExceptionNoSparesNoStrike() {
		//given
		BowlingGame game = new BowlingGame();
		boolean exceptionCaught=false;
		try{
			for(int i=0; i<21 ; i++)
			{
				game.roll(1);
			}//when
		}catch(Exception e)
		{
			exceptionCaught=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void shouldGiveTheExtraBallStrike() {
		//given
		BowlingGame game = new BowlingGame();
		//when
		for(int i=0; i <12 ; i++)
		{
			game.roll(10);
		}
		
		boolean result=game.isFinished();
		//then
		assertEquals(true,result);
	}

	@Test
	public void shouldGiveTheExtraBallSpare() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <9 ; i++)
		{
			game.roll(10);
		}
		game.roll(8);
		game.roll(2);
		game.roll(10);
		//when
		boolean result=game.isFinished();
		//then
		assertEquals(true,result);
	}
	
	@Test
	public void shouldCountScoreNoBonus() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(1);
		game.roll(2);
		game.roll(3);
		game.roll(4);
		//when
		int result=game.score();
		//then
		assertEquals(10,result);
	}
	
	@Test
	public void shouldCountScoreSpare() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(1);
		game.roll(9);
		game.roll(3);
		game.roll(2);
		//when
		int result=game.score();
		//then
		assertEquals(18,result);
	}
	
	@Test
	public void shouldCountScoreStrike() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(10);
		game.roll(9);
		game.roll(0);
		game.roll(2);
		//when
		int result=game.score();
		//then
		assertEquals(30,result);
	}
	
	@Test
	public void shouldCountConsecutiveStrikes() {
		//given
		BowlingGame game = new BowlingGame();
		game.roll(10);//    23
		game.roll(10);//x2  15
		game.roll(3);//x3   3
		game.roll(2);//x2   2
		game.roll(1);//x1   1
		//when
		int result=game.score();
		//then
		assertEquals(44,result);
	}
	
	@Test
	public void shouldCountPerfect() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <12 ; i++)
		{
			game.roll(10);
		}
		//when
		int result=game.score();
		//then
		assertEquals(300,result);
	}
	
	@Test
	public void shouldRecognizeBrokenLastFrame() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <9 ; i++)
		{
			game.roll(10);
		}
		game.roll(5);
		boolean exceptionCaught=false;
		//when
		try {
			game.roll(8);
		}
		catch(Exception e) {
			System.out.println(e.toString());
			exceptionCaught=true;
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void shouldRecognizeBrokenLastFrameStrike() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <10 ; i++)
		{
			game.roll(10);
		}
		game.roll(5);
		boolean exceptionCaught=false;
		//when
		try {
			game.roll(8);
		}
		catch(Exception e) {
			System.out.println(e.toString());
			exceptionCaught=true;
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void shouldNotThrowExceptionLastFrameSpare() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <9; i++)
		{
			game.roll(10);
		}
		
		game.roll(5);
		game.roll(5);
		//Spare! Extra ball should count
		boolean exceptionCaught=false;
		//when
		try {
			game.roll(8);
		}
		catch(Exception e) {
			System.out.println(e.toString());
			exceptionCaught=true;
		}
		//then
		assertEquals(false,exceptionCaught);
	}
	
	@Test
	public void shouldCountConsecutiveSpares() {
		//given
		BowlingGame game = new BowlingGame();
		for(int i=0; i <21 ; i++)
		{
			game.roll(5);
		}
		//when
		int result=game.score();
		//then
		assertEquals(150,result);
	}
	
	
}
