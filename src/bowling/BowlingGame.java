package bowling;

public class BowlingGame implements BowlingGameResultCalculator {
	private int frame;
	private int ball;
	private int[] scoreTable;//minimal approach: to count total score one needs to remember only the current frame
	private boolean end;
	private boolean strike;
	private boolean spare;
	private int bonus;//after spare or strike sequence some results count twice or thrice
	private int gameScore;//total score, updated after each ball
	
	public BowlingGame() {
		frame =0;
		ball=0;
		scoreTable = new int[2];
		for(int i=0; i <scoreTable.length; i++)
			scoreTable[i]=0;
		end = false;
		strike=false;
		spare=false;
		bonus=0;
		gameScore=0;
	}
	
	@Override
	public void roll(int numberOfPins) {
		if(isFinished())
			throw new IllegalStateException("Game Over!");
		else if ((numberOfPins<0)||(numberOfPins>10))
			throw new IllegalArgumentException("You can only knock 0-10 pins with one ball.");
		else
		{
			if(frame<9)
				doRegularFrameUpdate(numberOfPins);
		else doFinalFrameUpdate(numberOfPins);
		}
	}
	
	@Override
	public int score() {
		return gameScore;
	}
	
	@Override
	public boolean isFinished() {
		return end;
	}
	
	public int getFrame() {
		return frame+1;
	}
	
	public String printTable() {
		String result = new String("");
		for(int i = 0; i< scoreTable.length; i++)
			result+=scoreTable[i]+" ";
		return result;
	}
	
	private void doRegularFrameUpdate(int numberOfPins) {
		spare=false;
		scoreTable[ball]= numberOfPins;
		updateResult(numberOfPins);
		setStrike();
		ball++;
		if(ball==2) {
			setSpare();
			checkFrameIntegrity();
		}
			
		setBonus();	
		if(strike||(ball>1))
			resetFrame();
	}

	private void checkFrameIntegrity() {
		// TODO Auto-generated method stub
		if ( (scoreTable[0]+scoreTable[1])>10) throw new IllegalStateException("Broken frame");
	}

	private void resetFrame() {
		{
			frame++;
			ball=0;
			scoreTable[0]=0;
			scoreTable[1]=0;
		}
	}
	
	private void doFinalFrameUpdate(int numberOfPins) {
		scoreTable[ball%2]= numberOfPins;//can get an extra ball
		updateResult(numberOfPins);
		setStrike();
		ball++;
		if(ball==2){
			
			setSpare();
			
		}
		if(ball>1 && !strike && !spare) checkFrameIntegrity();
		
		if ((ball>2) || ((ball>1)&&(!strike)&&(!spare))  ) end=true;
	}
	
	private void setStrike() {
		strike=( scoreTable[0]==10);
	}
	
	private void setSpare() {
		spare=( (scoreTable[0]+scoreTable[1])==10);
	}
	
	public boolean getStrike() {
		return strike;
	}
	
	public boolean getSpare() {
		return spare;
	}
	
	private void setBonus(){
		if (strike) bonus+=2;
		if (spare) bonus+=1;
	}
	
	public int getBonus(){
		return bonus;
	}
	
	private void updateResult(int numberOfPins)
	{
		gameScore+= numberOfPins;
		if((bonus==1)||(bonus==2)) { //single bonus after strike or spare
			gameScore+= numberOfPins;
			bonus-=1;
		}
		else if(bonus==3) {//double bonus after two consecutive strikes
			gameScore+= 2*numberOfPins;
			bonus-=2;
		}
	}
	
}
